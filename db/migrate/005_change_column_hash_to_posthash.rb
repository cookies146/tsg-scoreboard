class ChangeColumnHashToPosthash < ActiveRecord::Migration
  def change
    remove_column :posts, :hash, :string
    add_column :posts, :posthash, :string
  end
end
