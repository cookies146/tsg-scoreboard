class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :name
      t.string  :pass
      t.boolean :disabled

      t.timestamps
    end

    add_index :users, :name, unique: true
  end
end
