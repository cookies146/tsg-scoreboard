class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.string  :genre
      t.string  :author
      t.string  :title
      t.text    :body
      t.string  :flag
      t.integer :point
      t.boolean :disabled

      t.timestamps
    end
  end
end
