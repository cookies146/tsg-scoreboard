class CreateTries < ActiveRecord::Migration
  def change
    create_table :tries do |t|
      t.integer :problem_id
      t.integer :user_id
      t.string  :try_flag
      t.string  :status

      t.timestamps
    end
  end
end
