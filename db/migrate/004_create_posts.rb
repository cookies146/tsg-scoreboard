class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string  :hash
      t.string  :title
      t.text    :content
      t.boolean :disabled

      t.timestamps
    end
  end
end
