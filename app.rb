require 'sinatra/base'
require 'sinatra/reloader'
require 'slim'
require 'slim/include'
require 'sass'

require_relative 'models/init'

raise 'no admin_pass' unless ENV['admin_pass']

class App < Sinatra::Base
  enable :sessions
  configure :development do
    register Sinatra::Reloader
  end

  before do
    pass if request.path_info =~ %r{^/(.*)\.css$}
    @error = session[:error] if session[:error]
    session[:error] = nil
  end
  get %r{^/(.*)\.css$} do
    scss :"styles/#{params[:captures].first}" #scssテンプレート
  end

  get '/' do
    slim :index, layout: :layout
  end

  get '/ranking' do
    @ranking = Ranking.new.ranking
    slim :ranking
  end

  get '/problem/:id' do
    @problemid = params[:id]
    @problem = Problem.where("id = ? AND disabled = ?", @problemid, false).take
    p @problem
    @problem ? slim(:problem) : 404
  end
  get '/problems' do
    @problems = Problem.where(disabled: false).order("showpid ASC")
    slim :problems
  end
  get '/post/:posthash' do
    post = Post.where('posthash = ? AND disabled = ?', params[:posthash], false).take
    return 404 unless post
    @title = post.title
    @content = post.content
    slim :post
  end

  post '/submit/:id' do
    @problemid = params[:id].to_i
    @username = params[:username] ? params[:username].to_s : nil
    @passhash = Helper.hash(params[:password].to_s)
    @flag = params[:flag].to_s

    @user = User.find_by(name: @username, pass: @passhash) if @username

    try = nil
    if @user
      unless try = Try.where("problem_id = ? AND user_id = ? AND status = 'OK'", @problemid, @user.id).take
        try = Submit.new(pid: @problemid, uid: @user.id, flag: @flag).submit
      end
    else
      try = Submit.new(pid: @problemid, flag: @flag).submit
    end

    if try.status == 'OK'
      if @user
        slim :flag_gotpoint
      else
        @error = 'ユーザ名またはパスワードが間違っています。' if @username
        slim :flag_accept
      end
    else
      session[:error] = 'フラグが違います...'
      redirect "/problem/#{@problemid}"
    end
  end

  get '/register' do
    slim :register, layout: :layout
  end

  post '/register' do
    @error = Register.new(params[:username], params[:password]).reg
    @error ?  slim(:register, layout: :layout) : redirect('/')
  end

  helpers do
    def admin_authenticate!
      redirect('/admin') unless session[:admin] == true
    end
  end
  before '/admin/*' do
    admin_authenticate!
  end
  get '/admin' do
    slim :login_admin, layout: :layout
  end
  post '/admin' do
    if String === params[:pass] && params[:pass] == ENV['admin_pass']
      session[:admin] = true
      redirect('/admin/')
    else
      redirect('/admin')
    end
  end
  get '/admin/' do
    slim :'admin/index', layout: :layout
  end
  get '/admin/post' do
    slim :'admin/post', layout: :layout
  end
  get '/admin/post/:id' do
    post = Post.find_by(id: params[:id]) || redirect('/admin/post')
    @posthash, @title, @content, @disabled = 
      post.posthash, post.title, post.content, post.disabled
    @postid = post.id
    slim :'admin/editpost', layout: :layout
  end
  post '/admin/post' do
    @posthash, @title, @content, @disabled = 
      params[:posthash], params[:title], params[:content], params[:disabled]
    @error = (post = Post.create(posthash: @posthash, title: @title, content: @content, disabled: @disabled=='on')) ? nil : post.errors.messages
    @error ? slim(:'admin/editpost', layout: :layout) : redirect('/admin/post')
  end
  post '/admin/post/:id' do
    @posthash, @title, @content, @disabled = 
      params[:posthash], params[:title], params[:content], params[:disabled]
    post = Post.find_by(id: params[:id])
    return 404 unless post
    @error = post.update(posthash: @posthash, title: @title, content: @content, disabled: @disabled=='on') ? nil : post.errors.messages
    @error ? slim(:'admin/editpost', layout: :layout) : redirect('/admin/post')
  end
  get '/admin/problem' do
    slim :'admin/problems', layout: :layout
  end
  get '/admin/problem/:id' do
    problem = Problem.find_by(id: params[:id]) || redirect('/admin/problem')
    @showpid, @genre, @author, @title, @body, @flag, @point, @disabled = 
      problem.showpid, problem.genre, problem.author, problem.title, problem.body, problem.flag, problem.point, problem.disabled
    @problemid = problem.id
    slim :'admin/editproblem', layout: :layout
  end
  post '/admin/problem/:id' do
    @showpid, @genre, @author, @title, @body, @flag, @point, @disabled, @problemid =
      params[:showpid], params[:genre], params[:author], params[:title], params[:body], params[:flag], params[:point], params[:disabled], params[:id]
    @error = ProblemRegister.new(params[:showpid], params[:genre], params[:author], params[:title], params[:body], params[:flag], params[:point], params[:disabled]).update(params[:id])
    @error ? slim(:'admin/editproblem', layout: :layout) : redirect('/admin/problem')
  end
  post '/admin/problem' do
    @showpid, @genre, @author, @title, @body, @flag, @point, @disabled = 
      params[:showpid], params[:genre], params[:author], params[:title], params[:body], params[:flag], params[:point], params[:disabled]
    @error = ProblemRegister.new(params[:showpid], params[:genre], params[:author], params[:title], params[:body], params[:flag], params[:point], params[:disabled]).register
    @error ? slim(:'admin/editproblem', layout: :layout) : redirect('/admin/problem')
  end
end
