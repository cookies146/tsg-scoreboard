require_relative 'database'
require_relative 'helper'

class Register
  def initialize (username, password)
    @username = username
    @password = password
  end

  def reg
    return 'invalid username' unless String === @username
    return 'invalid password' unless String === @password
    return 'パスワードが短すぎます。' unless @password.size >= 4

    newuser = User.new
    newuser.name = @username
    newuser.pass = Helper.hash( @password )
    newuser.disabled = false
    unless newuser.save
      newuser.errors.messages
    else
      Try.create!(user_id: User.find_by(name: @username).id, problem_id: Problem.find_by(title: 'dummy problem'), status: 'OK')
      nil
    end

  end
end
