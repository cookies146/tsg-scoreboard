require 'active_record'
require 'yaml'
config = YAML.load_file('config/database.yml')
ActiveRecord::Base.establish_connection(ENV['DATABASE_URL'] || config[ENV['RACK_ENV']])

class User < ActiveRecord::Base
  has_many :tries

  validates :name, format: { with: /\A(?:\w| |\p{Hiragana}|\p{Katakana}|[ー－]|[一-龠々])+\z/, message: 'ひらがな、カタカナ、漢字、英数字のみが使用できます。'}
  validates :name, uniqueness: { case_sensitive: false, message: 'このユーザ名はすでに使われています。' }
end

class Problem < ActiveRecord::Base
  has_many :tries
end

class Try < ActiveRecord::Base
  belongs_to :user
  belongs_to :problem
end

class Post < ActiveRecord::Base; end
