require_relative 'database'
class Submit
  def initialize(args)
    @pid = args[:pid].to_i
    @uid = args[:uid] || User.find_by(name: 'anonymous').id
    @flag= args[:flag]
  end

  def submit
    try = Try.new
    try.problem_id = @pid
    try.user_id = @uid
    try.try_flag = @flag
    try.status = (validate ? 'OK' : 'NG')
    try.save!
    try
  end

  protected
  def validate
    problem = Problem.find_by(id: @pid)
    raise ArgumentError, 'invalid problem id' unless problem
    flag = problem.flag
    flag == @flag
  end
end
