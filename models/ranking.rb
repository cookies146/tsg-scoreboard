require_relative 'database'

class Ranking
  def initialize
  end

  def ranking
    Try.joins(:problem, :user).connection.select_all(<<-SQL).rows
      SELECT users.name, SUM( problems.point ) AS sum_point FROM users, problems, tries
      WHERE tries.user_id = users.id AND tries.problem_id = problems.id AND
            users.name != 'anonymous' AND tries.status = 'OK'
      GROUP BY users.id
      ORDER BY sum_point DESC
    SQL
  end
end
