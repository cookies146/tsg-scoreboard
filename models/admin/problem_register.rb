require_relative '../database'

class ProblemRegister
  def initialize(showpid, genre, author, title, body, flag, point, disabled)
    @showpid, @genre, @author, @title, @body, @flag, @point, @disabled =
      showpid.to_i, genre, author, title, body, flag, point.to_i, !!disabled
  end

  def register
    newproblem = Problem.new
    newproblem.showpid = @showpid
    newproblem.genre = @genre
    newproblem.author= @author
    newproblem.title = @title
    newproblem.body  = @body
    newproblem.flag  = @flag
    newproblem.point = @point
    newproblem.disabled = @disabled
    return newproblem.errors.messages unless newproblem.save
    nil
  end

  def update(id)
    updateproblem = Problem.find_by(id: id)
    return 'invalid id' unless updateproblem
    
    updateproblem.update(
      showpid: @showpid,
      genre: @genre,
      author: @author,
      title: @title,
      body: @body,
      flag: @flag,
      point: @point,
      disabled: @disabled
    )
    nil
  end
end
